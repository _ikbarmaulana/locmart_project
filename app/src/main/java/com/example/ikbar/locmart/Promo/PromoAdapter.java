package com.example.ikbar.locmart.Promo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.ikbar.locmart.Models.Promo;
import com.example.ikbar.locmart.R;

import java.util.ArrayList;

import retrofit2.Callback;

import static android.content.ContentValues.TAG;

/**
 * Created by IKBAR on 11/22/2017.
 */

public class PromoAdapter extends RecyclerView.Adapter<PromoAdapter.MyViewHolder> {
    private Context pContext;
    private ArrayList<Promo> promoList;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public ImageView img_promo;

        public MyViewHolder(View v){
            super(v);
            img_promo = (ImageView) v.findViewById(R.id.img_promo);
        }
    }

    public PromoAdapter(Context context, ArrayList<Promo> obj){
        this.pContext = context;
        this.promoList = obj;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.all_promo_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        String url = pContext.getString(R.string.BASE_URL);

        Glide.with(pContext).load(url  + "backend/web/" + promoList.get(position).getPromo_img()).into(holder.img_promo);
//        Glide.with(pContext).load(promo.getPromo_img()).into(holder.img_promo);
        Log.d(TAG, "onBindViewHolder: " + url + "backend/web/" + promoList.get(position).getPromo_img());
    }

    @Override
    public int getItemCount() {
        return (promoList==null) ? 0 : promoList.size();
    }
}
