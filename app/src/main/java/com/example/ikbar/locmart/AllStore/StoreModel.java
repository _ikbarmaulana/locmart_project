package com.example.ikbar.locmart.AllStore;

/**
 * Created by IKBAR on 11/21/2017.
 */

public class StoreModel {
    private String storeName;
    private int imgStore, id;

    public StoreModel(){}

    public StoreModel(int id, String storeName, int imgStore){
        this.id = id;
        this.storeName = storeName;
        this.imgStore = imgStore;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public int getImgStore() {
        return imgStore;
    }

    public void setImgStore(int imgStore) {
        this.imgStore = imgStore;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
