package com.example.ikbar.locmart.Promo;

/**
 * Created by IKBAR on 11/22/2017.
 */

public class PromoModel {
    private String promoName;
    private int img_promo;

    public PromoModel(){

    }

    public PromoModel(String promoName, int img_promo){
        this.promoName = promoName;
        this.img_promo = img_promo;
    }

    public String getPromoName() {
        return promoName;
    }

    public void setPromoName(String promoName) {
        this.promoName = promoName;
    }

    public int getImg_promo() {
        return img_promo;
    }

    public void setImg_promo(int img_promo) {
        this.img_promo = img_promo;
    }
}
