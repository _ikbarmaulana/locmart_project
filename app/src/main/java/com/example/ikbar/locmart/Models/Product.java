package com.example.ikbar.locmart.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Escanor on 12/20/2017.
 */

public class Product {
    @SerializedName("PRODUCT_ID")
    private String product_id;
    @SerializedName("CATEGORY_ID")
    private String category_id;
    @SerializedName("STORE_ID")
    private String store_id;
    @SerializedName("PRODUCT_NAME")
    private String product_name;
    @SerializedName("PRODUCT_PRICE")
    private String product_price;
    @SerializedName("PRODUCT_DESCRIPTION")
    private String product_description;
    @SerializedName("PRODUCT_IMG")
    private String product_img;

    public String getProduct_id() {
        return product_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public String getProduct_price() {
        return product_price;
    }

    public String getProduct_description() {
        return product_description;
    }

    public String getProduct_img() {
        return product_img;
    }

    public String getStore_id() {
        return store_id;
    }
}
