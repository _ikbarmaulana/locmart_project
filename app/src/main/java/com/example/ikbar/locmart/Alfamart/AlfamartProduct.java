package com.example.ikbar.locmart.Alfamart;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.ikbar.locmart.AllProduct.ProductModel;
import com.example.ikbar.locmart.AllProduct.ProductAdapter;
import com.example.ikbar.locmart.LocationAdapter;
import com.example.ikbar.locmart.Models.Product;
import com.example.ikbar.locmart.Models.Category;
import com.example.ikbar.locmart.Models.StoreLocation;
import com.example.ikbar.locmart.R;
import com.example.ikbar.locmart.Superindo.SuperindoLocation;
import com.example.ikbar.locmart.apihelper.BaseApiService;
import com.example.ikbar.locmart.apihelper.UtilsApi;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class AlfamartProduct extends Fragment {
    EditText search_alfa;
    Spinner spinner;
    Context context;
    private RecyclerView recyclerView;
    private ProductAdapter adapter;
    private ArrayList<Product> productList;
    private ArrayList<Category> categoryList;
    BaseApiService mApiService;
    Integer spin;
    Integer storeId=2;

    public static AlfamartProduct newInstance() {

        return new AlfamartProduct();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.alfamart_product, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_product_alfa);
        productList = new ArrayList<>();
        adapter = new ProductAdapter(getContext(), productList);
        mApiService = UtilsApi.getAPIService(getContext());

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


//        spinner = (Spinner) v.findViewById(R.id.spinner_alfa);
//        search_alfa = (EditText) v.findViewById(R.id.search_alfa);
//
//        List<String> categories = new ArrayList<>();
//        categories.add("Food");
//        categories.add("Drink");
//        categories.add("Cigarette");
//        categories.add("Healthy & Beauty");
//        categories.add("Household Appliance");
//        categories.add("Toys");
//        categories.add("Stationary");
//        categories.add("Mom & Kids");
//
//        //creat adapter spinner
//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(v.getContext(), android.R.layout.simple_spinner_dropdown_item, categories);
//
//        //dropdown layout style
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//        //attaching data adapter to spinner
//        spinner.setAdapter(dataAdapter);
//
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
//
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String item = parent.getItemAtPosition(position).toString();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//
//        });
//
//        search_alfa.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//
//        spinner = (Spinner) v.findViewById(R.id.spinner_alfa);
//        search_alfa = (EditText) v.findViewById(R.id.search_alfa);

        final ArrayList<Integer> categoriesId = new ArrayList<>();
        final ArrayList<String> categoriesName = new ArrayList<>();

        final Call<ArrayList<Category>> categoryCall = mApiService.category();
        categoryCall.enqueue(new Callback<ArrayList<Category>>() {
            @Override
            public void onResponse(Call<ArrayList<Category>> call, Response<ArrayList<Category>> response) {
                try {
                    categoryList = response.body();
                    Log.d(TAG, "categoryList : " + categoryList.get(0).getCategory_name());
                }catch (Exception e){
                    e.printStackTrace();
                }

                for (int i=0; i<categoryList.size(); i++){
                    categoriesId.add(categoryList.get(i).getCategory_id());
                    categoriesName.add(categoryList.get(i).getCategory_name());
                }

//                adapter = new ProductAdapter(getContext(), productList);
//
//                RecyclerView.LayoutManager lLayoutManager = new GridLayoutManager(getActivity(), 2);
//                recyclerView.setLayoutManager(lLayoutManager);
//                recyclerView.addItemDecoration(new AlfamartProduct.GridSpacingItemDecoration(2, dpToPx(10), true));
//                recyclerView.setItemAnimator(new DefaultItemAnimator());
//                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<ArrayList<Category>> call, Throwable t) {
                t.printStackTrace();
            }
        });

        //creat adapter spinner
//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(v.getContext(), android.R.layout.simple_spinner_dropdown_item,categoriesName);

        //dropdown layout style
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //attaching data adapter to spinner
//        spinner.setAdapter(dataAdapter);
//
//       spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//           @Override
//           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//               spin = categoriesId.get(position);
//
//               final Call<ArrayList<Product>> productCall = mApiService.productAlfamart(storeId, spin);
//               productCall.enqueue(new Callback<ArrayList<Product>>() {
//                   @Override
//                   public void onResponse(Call<ArrayList<Product>> call, Response<ArrayList<Product>> response) {
//                       try {
//                           productList = response.body();
//                           Log.d(TAG, "productList: " + productList);
//                       }catch (Exception e){
//                           e.printStackTrace();
//                       }
//
//                       adapter = new ProductAdapter(getContext(), productList);
//
//                       RecyclerView.LayoutManager lLayoutManager = new GridLayoutManager(getActivity(), 2);
//                       recyclerView.setLayoutManager(lLayoutManager);
//                       recyclerView.addItemDecoration(new AlfamartProduct.GridSpacingItemDecoration(2, dpToPx(10), true));
//                       recyclerView.setItemAnimator(new DefaultItemAnimator());
//                       recyclerView.setAdapter(adapter);
//                   }
//
//                   @Override
//                   public void onFailure(Call<ArrayList<Product>> call, Throwable t) {
//                       t.printStackTrace();
//                   }
//               });
//
//           }
//
//           @Override
//           public void onNothingSelected(AdapterView<?> parent) {
//
//           }
//       });

//        search_alfa.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });

        final Call<ArrayList<Product>> productCall = mApiService.products(storeId);
        productCall.enqueue(new Callback<ArrayList<Product>>() {
            @Override
            public void onResponse(Call<ArrayList<Product>> call, Response<ArrayList<Product>> response) {
                try {
                    productList = response.body();
                    Log.d(TAG, "productList: " + productList);
                }catch (Exception e){
                    e.printStackTrace();
                }

                adapter = new ProductAdapter(getContext(), productList);

                RecyclerView.LayoutManager lLayoutManager = new GridLayoutManager(getActivity(), 2);
                recyclerView.setLayoutManager(lLayoutManager);
                recyclerView.addItemDecoration(new AlfamartProduct.GridSpacingItemDecoration(2, dpToPx(10), true));
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ArrayList<Product>> call, Throwable t) {
                t.printStackTrace();
            }
        });

        return v;
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
