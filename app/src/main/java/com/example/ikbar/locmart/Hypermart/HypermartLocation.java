package com.example.ikbar.locmart.Hypermart;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ikbar.locmart.LocationAdapter;
import com.example.ikbar.locmart.Models.StoreLocation;
import com.example.ikbar.locmart.R;
import com.example.ikbar.locmart.Superindo.SuperindoLocation;
import com.example.ikbar.locmart.apihelper.BaseApiService;
import com.example.ikbar.locmart.apihelper.UtilsApi;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IKBAR on 10/11/2017.
 */

public class HypermartLocation extends Fragment {
    public static HypermartLocation newInstance(){
        return new HypermartLocation();
    }

    private RecyclerView recyclerView;
    private LocationAdapter locationAdapter;
    BaseApiService mApiService;
    ArrayList<StoreLocation> coordinate;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hypermart_location, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.hyper_location);

        mApiService = UtilsApi.getAPIService(getContext());
        Call<ArrayList<StoreLocation>> storeLocationCall = mApiService.locationHyper();
        storeLocationCall.enqueue(new Callback<ArrayList<StoreLocation>>(){
            @Override
            public void onResponse(Call<ArrayList<StoreLocation>> call, Response<ArrayList<StoreLocation>> response) {
                try {

                    coordinate = response.body();
                    Log.d("Coordinate", "onResponse: " + coordinate.get(0).getCoordinate());
                }catch (Exception e){
                    e.printStackTrace();
                }

                locationAdapter = new LocationAdapter(coordinate);

                RecyclerView.LayoutManager lLayoutManager = new GridLayoutManager(getActivity(), 1);
                recyclerView.setLayoutManager(lLayoutManager);
                recyclerView.addItemDecoration(new HypermartLocation.GridSpacingItemDecoration(1, dpToPx(10), true));
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(locationAdapter);
            }

            @Override
            public void onFailure(Call<ArrayList<StoreLocation>> call, Throwable t) {
                t.printStackTrace();
            }
        });

        return view;
    }


    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}